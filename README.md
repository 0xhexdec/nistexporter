
# NistExporter

This is a very simple program to help with exporting values from the nist website.

## Requirements

To run the program you need Python 3 (Tested and developed with Python 3.8.1)

## Use

To see all possible arguments, use

      >python nistExporter --help

      Running version from 19.05.2020, 18:50
      usage: nistExporter.py [-h] [-p] [-v] [-f FILE] [-o NAME] [--id FluidID] [--min TEMP] [--max TEMP] [--inc TEMP] [--temps FILE]

      Downloads, parses and cleans material properties from nist to generate Mortran arrays

      optional arguments:
      -h, --help            show this help message and exit
      -p, --printvariables  prints all usable variables in the master file
      -v, --verbose         activates the verbose output
      -f FILE, --file FILE  set the mortan master file to use, if not specified the output will be generated in a blank file
      -o NAME, --output NAME
                            sets the name for the output file, if the file already exists, the file gets overwritten
      --id FluidID          set the fluidID and overwrite the value specified in the master file (default: C124389)
      --min TEMP            set the minimal pressure and overwrite values specified in the master file (default: 1)
      --max TEMP            set the maximal pressure and overwrite the value specified in the master file (default: 205.0)
      --inc TEMP            set the increase in pressure every step and overwrite the value specified in the master file (default: 1)
      --temps FILE          provide a CSV with your desired temperatures, this overwrites values specified in the master file (default=[221.6])
